var seccion = document.getElementsByTagName('div');
var seccionUnica = document.getElementById('seccion-unica');
var seccionGenerica = document.getElementsByClassName('seccion-generica');
var seccionDespedida = document.getElementsByName('despedida');
console.log('seccion', seccion);
console.log('seccion unica', seccionUnica);
console.log('seccion generica', seccionGenerica);
console.log('seccion despedida', seccionDespedida);

seccion[0].id = "seccion-inicial";
seccion[1].hidden = true;
seccion[2].id = "seccion-generica-1";
seccion[0].style = "background: black; color: white;";

var parrafos = document.getElementsByTagName('p');
parrafos[1].innerHTML = "<h1>Titulo</h1>";
parrafos[1].style = "color: red;";