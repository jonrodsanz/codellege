function Mascota (lluvia, sonido){
    this.raining = lluvia,
    this.noise = sonido,
    this.makeNoise = function(){
        if(this.raining === true){
            console.log(this.noise);
        }
    }
};

var gato = new Mascota(true, "Miaw!");
var perro = new Mascota(true, "Wof!");

if(gato.raining === true && perro.raining === true){
    console.log("WARNING! MASS ATACK!");
}

function Personaje(nombre, genero, profesion, edad, fuerza, hp, printstats){
  this.nombre = nombre,
  this.genero = genero, 
  this.profesion = profesion,
  this.edad = edad,
  this.fuerza = fuerza,
  this.hitpoints = hp,
  this.printstats = function (){
    console.log("Nombre: " + this.nombre + "\n" + "Género: " + this.genero + "\n" + "Profesión: " + this.profesion + "\n" + "Edad: " + this.edad + "\n" + "Fuerza: " + this.fuerza + "\n" + "HitPoints(HP): " + this.hitpoints + "\n");
  }
};

var jonathan = new Personaje("Jonathan", "Masculino", 17, "Estudiante", 45, 100);
jonathan.printstats();
var arquero = new Personaje("Casie", "Femenino", "Arquero", 25, 70, 80);
arquero.printstats();