//SETUP
    //ARREGLOS CON CONTENIDO
    var arrayDeNavegadores = [
        {
            nombre: 'Firefox',
            imagen: "https://cdn1.iconfinder.com/data/icons/smallicons-logotypes/32/firefox-512.png",
            fondo: "https://hellofuture.orange.com/app/uploads/2018/01/Intro-HELLO-FUTURE-1920x1080_v2.gif",
            descripcion: "Mozilla Firefox es un navegador web libre y de código abierto​ desarrollado para Linux, Android, IOS, OS X y Microsoft Windows coordinado por la Corporación Mozilla y la Fundación Mozilla."
        },
        {
            nombre: 'Opera',
            imagen: "https://cdn.icon-icons.com/icons2/1495/PNG/512/opera_103194.png",
            fondo: "https://wallpapercave.com/wp/JUM2W6x.jpg",
            descripcion: "Opera es un navegador web creado por la empresa noruega Opera Software. Usa el motor de renderizado Blink.​ Tiene versiones para computadoras de escritorio, teléfonos móviles y tabletas."
        },
        {
            nombre: 'Microsoft Edge',
            imagen: "https://png.icons8.com/dusk/1600/ms-edge.png",
            fondo: "https://cdn.allwallpaper.in/wallpapers/2400x1350/16571/computers-computer-technology-data-center-science-server-2400x1350-wallpaper.jpg",
            descripcion: "Microsoft Edge es un navegador web desarrollado por Microsoft, que se encuentra incluido en Windows 10, donde reemplazó a Internet Explorer como navegador web preestablecido."
        },
        {
            nombre: 'Chrome',
            imagen: "http://www.iconarchive.com/download/i61935/appicns/simplified-app/appicns-Chrome.ico",
            fondo: "https://www.internationalfinance.com/wp-content/uploads/2017/10/iStock-855932900.jpg",
            descripcion: "Google Chrome es un navegador web de software privativo o código cerrado​​ desarrollado por Google, aunque derivado de proyectos de código abierto."
        },
        {
            nombre: 'Safari',
            imagen: "https://cdn1.iconfinder.com/data/icons/style-4-stock/807/Safari_S4-01.png",
            fondo: "https://wallpaper-house.com/data/out/8/wallpaper2you_258915.jpg",
            descripcion: "Safari es un navegador web de código cerrado desarrollado por Apple Inc. Está disponible para macOS, iOS"
        }
    ];
    var arregloDeSistemasOperativos = [
        {
            nombre: "Windows",
            imagen: "https://cdn1.iconfinder.com/data/icons/smallicons-logotypes/32/microsoft-512.png",
            fondo: "https://img00.deviantart.net/0e33/i/2016/052/1/2/windows_10_minimal_wallpaper_by_nikhilkaushik-d9skjze.jpg",
            descripcion: "Microsoft Windows es el nombre de una familia de distribuciones de software para PC, smartphone, servidores y sistemas empotrados, desarrollados y vendidos por Microsoft y disponibles para múltiples arquitecturas, tales como x86, x86-64 y ARM."
        },
        {
            nombre: "Linux",
            imagen: "http://pngimg.com/uploads/linux/linux_PNG1.png",
            fondo: "http://hdqwalls.com/wallpapers/linux-terminal-commands.jpg",
            descripcion: "Linux es una familia de sistemas operativos de software libre y de código abierto construidos alrededor del kernel de Linux. Típicamente, Linux se empaqueta en una forma conocida como distribución de Linux para uso de escritorio y servidor."
        },
        {
            nombre: "macOS",
            imagen: "https://upload.wikimedia.org/wikipedia/commons/thumb/b/bb/OS_X_El_Capitan_logo.svg/1024px-OS_X_El_Capitan_logo.svg.png",
            fondo: "http://media.idownloadblog.com/wp-content/uploads/2016/06/macOS-Sierra-Wallpaper-Macbook-Wallpaper.jpg",
            descripcion: "macOS es una serie de sistemas operativos gráficos desarrollados y comercializados por Apple Inc. desde 2001. Es el sistema operativo principal para la familia de computadoras Mac de Apple."
        }
    ];
    var arregloDeProcesadores = [
        {
            nombre: "Intel Core i9",
            imagen: "https://www.overclockers.co.uk/media/image/thumbnail/CP63AIN_168078_800x800.png",
            fondo: "https://i.pinimg.com/originals/ac/98/62/ac9862ed9501757888707fd453ab8088.jpg",
            descripcion: "La plataforma móvil Intel® de primer nivel con memoria Intel® Optane™ integrada que permite liberar la potencia de su computadora para que sea más adaptable y obtener una capacidad de respuesta mayor para las tareas cotidianas."
        },
        {
            nombre: "AMD Ryzen 7",
            imagen: "https://cdn3.centrecom.com.au/images/upload/0041311_0.png",
            fondo: "http://www.desktop-screens.com/data/out/4/2501897-amd-wallpapers.png",
            descripcion: "Ryzen es una línea de microprocesadores de la empresa AMD. La marca se introdujo en el año 2017 con productos que incorporan la microarquitectura Zen."
        }
    ];

    //FUNCION PARA CREAR, AGREGAR CONTENIDO Y ANIDAR LOS ELEMENTOS DEL array EN EL ARCHIVO HTML
function template(array){
    var body = document.getElementsByTagName("body")[0];
    var container = document.getElementById("container");
    body.appendChild(container);
    for (var i=0; i< array.length; i++){
        var elemento =  array[i];
            
        var section = document.createElement("section");
        section.setAttribute("id", elemento.nombre);
        section.style = "background-image: url(" + elemento.fondo + "); background-size: cover; background-attachment: fixed; background-repeat: no-repeat; height: 100vh; width: 100vw; background-position: center;";

        var titulo = document.createElement("h2"); 
        titulo.innerHTML = elemento.nombre;
        titulo.style = "text-align: center;";

        var imagen = document.createElement("img");
        imagen.src = elemento.imagen;
            
        var descripcion =  document.createElement("div");
        var parrafo = document.createElement("p");
            
        parrafo.innerHTML = elemento.descripcion;

        container.appendChild(section);
        section.appendChild(titulo);
        section.appendChild(imagen);
        section.appendChild(descripcion);
        descripcion.appendChild(parrafo);
    }
}

template(arrayDeNavegadores);


//INTERACION CON EL USUARIO

    //OBTENCION DEL SIDEBAR Y NAVBAR 
    var sidebar = document.getElementById("sidebar");
    var navbar = document.getElementById("navbar");
    //OBTENCION DE BOTONES OPEN Y CLOSE PARA EL SIDEBAR
    var open = document.getElementById("open-sidebar");
    var close = document.getElementById("close-sidebar");

    //FUNCION PARA DESPLEGAR LA SIDEBAR A EJECUTAR AL CLICKEAR EL BOTON open
    open.addEventListener("click", function (){
        sidebar.style.width = "15vw";
        document.getElementsByTagName("section")[0].style.visibility = "visible";
    });
    //FUNCION PARA OCULTAR LA SIDEBAR A EJECUTAR AL CLICKEAR EL BOTON close
    close.addEventListener("click", function cerrarSidebar(){
        sidebar.style.width = "0";
        document.getElementsByTagName("section")[0].style.visibility = "hidden";
    });
    
    var sistemasOperativos = document.getElementById("OS");
    sistemasOperativos.addEventListener("click", function(){
        container.innerHTML = "";             //Borra todo el contenido de la pagina, excepto el navbar
        template(arregloDeSistemasOperativos);
        //cerrarSidebar();
    });
    var navegadores = document.getElementById("navegadores");
    navegadores.addEventListener("click", function(){
        container.innerHTML = "";             //Borra todo el contenido de la pagina, excepto el navbar
        template(arrayDeNavegadores);
        //cerrarSidebar();
    });
    var procesadores = document.getElementById("procesadores");
    procesadores.addEventListener("click", function(){
        container.innerHTML = "";             //Borra todo el contenido de la pagina, excepto el navbar
        template(arregloDeProcesadores);
        //cerrarSidebar();
    });
