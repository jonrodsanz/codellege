$(document).ready(function(){

    $("button").click(function(){

        var searchWord = $("input").val().trim();
        searchWord = searchWord.replace(/ /g, "+");
        var queryURL = 'http://api.giphy.com/v1/gifs/search?q=' + searchWord + '&api_key=dc6zaTOxFJmzC';

        $.getJSON(queryURL, function(serverResponse){
            document.getElementById("response").innerHTML = "";

            for(var i=0; i<24; i++){   
                var img = document.createElement("img");
                var id = "img-"+i;

                document.getElementById("response").appendChild(img);
                img.setAttribute("id", id);
                document.getElementById(id).setAttribute("src", serverResponse.data[i].images.fixed_height.url);    
            }

        });
    });
});